package solutions.rty.bao;

import android.app.Application;

import com.bugsnag.android.Bugsnag;
import com.github.pwittchen.networkevents.library.NetworkEvents;
import com.squareup.otto.Bus;

import solutions.rty.bao.data.DatabaseHelper;
import solutions.rty.bao.data.SessionManager;
import solutions.rty.bao.receiver.WebsocketManager;

//import quickutils.core.QuickUtils;

/**
 * Created by Yuri Heupa on 01/08/14.
 * Copyright (C) 2013 - All Rights Reserved
 */
public class BaoApplication extends Application {

    private static final String TAG = BaoApplication.class.getSimpleName();

    private static DatabaseHelper helper;

    public static DatabaseHelper getHelper() {
        return helper;
    }


    private static Bus bus;
    private static NetworkEvents networkEvents;

    @Override
    public void onCreate() {
        super.onCreate();
        Config.init(this);
        Bugsnag.init(this);
        // Instance to handle database operations
//        Bugsnag.notify(new RuntimeException("Non-fatal"));
        helper = new DatabaseHelper(this);
        SessionManager.init(this);
        bus = new Bus();
        networkEvents = new NetworkEvents(this, bus);
        networkEvents.register();
        WebsocketManager.init(Config.getWebsocketUrl(SessionManager.getActiveSession()), bus);
    }

    public static Bus getBus() {
        return bus;
    }

    public static NetworkEvents getNetworkEvents() {
        return networkEvents;
    }

}
