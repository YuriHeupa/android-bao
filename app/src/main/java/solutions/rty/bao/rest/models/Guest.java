package solutions.rty.bao.rest.models;

/**
 * GET!
 * solutions.rty.bao.rest.models
 * Created by Yuri Heupa on 19/03/15.
 * <p/>
 * Copyright (C) 2015 RTY Solutions - All Rights Reserved
 */
public class Guest {

    private int id;
    private String image;
    private boolean highlighted;

    public Guest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isHighlighted() {
        return highlighted;
    }

    public void setHighlighted(boolean highlighted) {
        this.highlighted = highlighted;
    }
}
