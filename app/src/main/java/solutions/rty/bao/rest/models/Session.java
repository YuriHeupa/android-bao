package solutions.rty.bao.rest.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Yuri Heupa on 15/12/14.
 * Copyright (C) 2013 - All Rights Reserved
 */
@DatabaseTable(tableName = "tb_session")
public class Session implements Parcelable {


    @DatabaseField(id = true, unique = true)
    int id;

    @DatabaseField
    private String token;

    @DatabaseField
    @JsonProperty(value = "device_id")
    private String deviceId;

    @DatabaseField
    @JsonProperty(value = "device_push")
    private String devicePushId;

    @DatabaseField
    @JsonIgnore
    private boolean notificationsEnabled = true;

    @DatabaseField
    @JsonProperty(value = "gender")
    private String myGender;

    @DatabaseField
    @JsonProperty(value = "target_gender")
    private String genderWanted;

    @JsonIgnore
    private String placeId;

    @DatabaseField
    private String lastChatPicture;

    public Session(){}

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }


    public boolean isNotificationsEnabled() {
        return notificationsEnabled;
    }

    public void setNotificationsEnabled(boolean notificationsEnabled) {
        this.notificationsEnabled = notificationsEnabled;

    }

    public String getMyGender() {
        return myGender;
    }

    public void setMyGender(String myGender) {
        this.myGender = myGender;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGenderWanted() {
        return genderWanted;
    }

    public void setGenderWanted(String genderWanted) {
        this.genderWanted = genderWanted;
    }

    public String getLastChatPicture() {
        return lastChatPicture;
    }

    public void setLastChatPicture(String lastChatPicture) {
        this.lastChatPicture = lastChatPicture;
    }

    public String getDevicePushId() {
        return devicePushId;
    }

    public void setDevicePushId(String devicePushId) {
        this.devicePushId = devicePushId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.token);
        dest.writeString(this.deviceId);
        dest.writeString(this.devicePushId);
        dest.writeByte(notificationsEnabled ? (byte) 1 : (byte) 0);
        dest.writeString(this.myGender);
        dest.writeString(this.genderWanted);
        dest.writeString(this.placeId);
        dest.writeString(this.lastChatPicture);
    }

    private Session(Parcel in) {
        this.id = in.readInt();
        this.token = in.readString();
        this.deviceId = in.readString();
        this.devicePushId = in.readString();
        this.notificationsEnabled = in.readByte() != 0;
        this.myGender = in.readString();
        this.genderWanted = in.readString();
        this.placeId = in.readString();
        this.lastChatPicture = in.readString();
    }

    public static final Creator<Session> CREATOR = new Creator<Session>() {
        public Session createFromParcel(Parcel source) {
            return new Session(source);
        }

        public Session[] newArray(int size) {
            return new Session[size];
        }
    };
}
