package solutions.rty.bao.rest.services;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;
import solutions.rty.bao.rest.models.foursquare.FoursquareDto;

public interface FoursquareService {

    @GET("/venues/search")
    void searchForVenues(
            @Query("client_id") String clientId,
            @Query("client_secret") String clientSecret,
            @Query("v") long apiDateVersion,
            @Query("ll") String coordinate,
            @Query("llAcc") float accuracy,
            @Query("query") String query,
            @Query("limit") int limit,
            @Query("intent") String intent,
            @Query("radius") int radius,
            @Query("categoryId") String[] categories,
            Callback<FoursquareDto> callback);

}