package solutions.rty.bao.rest.services;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;
import solutions.rty.bao.rest.models.Guest;
import solutions.rty.bao.rest.models.DeviceStatus;
import solutions.rty.bao.rest.models.OkStatus;
import solutions.rty.bao.rest.models.Session;

/**
 * Created by Yuri Heupa on 28/04/15.
 * <p/>
 * Copyright (C) 2015 RTY Solutions - All Rights Reserved
 */
public interface ApiService {

    @Multipart
    @POST("/api/device/register")
    void registerSession(
            @Query("id") String id,
            @Query("gender") String gender,
            @Query("interest") String interest,
            @Query("place") String place,
            @Query("push") String gcmtoken,
            @Part("picture") TypedFile picture,
            Callback<Session> callback);

    @GET("/api/session/contacts")
    void getPlaceGuests(
            @Header("Authorization") String token,
            @Query("place") String place,
            Callback<List<Guest>> callback);


    @GET("/api/device/status")
    void getDeviceStatus(
            @Query("id") String device,
            Callback<DeviceStatus> callback);


    @POST("/api/session/{id}/{action}")
    void highlightContact(
            @Header("Authorization") String token,
            @Path("id") int contactId,
            @Path("action") String action,
            Callback<OkStatus> callback);

    @POST("/api/match/confirm/{match_id}")
    void confirmMatch(
            @Header("Authorization") String token,
            @Path("match_id") int matchId,
            Callback<OkStatus> callback);


}