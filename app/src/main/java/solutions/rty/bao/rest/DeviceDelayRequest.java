package solutions.rty.bao.rest;

import android.app.Activity;
import android.content.Intent;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import solutions.rty.bao.data.SessionManager;
import solutions.rty.bao.rest.models.DeviceStatus;
import solutions.rty.bao.ui.activity.ChatActivity;
import solutions.rty.bao.ui.activity.DelayActivity;

/**
 * Bao
 * solutions.rty.bao.rest.requests
 * Created by Yuri Heupa on 06/04/15.
 * <p/>
 * Copyright (C) 2015 RTY Solutions - All Rights Reserved
 */
public class DeviceDelayRequest {

    public static void init(final ChatActivity activity) {

        BaoClient.getApiService()
                .getDeviceStatus(SessionManager.getActiveSession().getDeviceId(),
                        new Callback<DeviceStatus>() {
                            @Override
                            public void success(DeviceStatus deviceStatus, Response response) {
                                Intent intent = new Intent(activity, DelayActivity.class)
                                        .putExtra("time", deviceStatus.getBusyTime())
                                        .putExtra("total_time", deviceStatus.getBusyTotalTime())
                                        .putExtra("from", activity.getClass());

                                intent.putExtra("message", deviceStatus.getStatus());
                                intent.putExtra("from_chat", true);


                                activity.startActivityForResult(intent, 0);

                            }

                            @Override
                            public void failure(RetrofitError error) {
                                error.printStackTrace();
                            }
                        });
    }

    public interface DeviceDelayCallback {
        public void onDeviceDelayReceived(float delay);
    }

    public static void validate(final Activity activity) {
        validate(activity, null);
    }

        public static void validate(final Activity activity, final DeviceDelayCallback callback) {

        if(SessionManager.getActiveSession() == null)
            return;
//        System.out.println(Config.getDeviceStatus(SessionManager.getActiveSession().getDeviceId()));
//        System.out.println("Token " + SessionManager.getActiveSession().getToken());

        BaoClient.getApiService()
                .getDeviceStatus(SessionManager.getActiveSession().getDeviceId(),
                        new Callback<DeviceStatus>() {
                            @Override
                            public void success(DeviceStatus deviceStatus, Response response) {
                                if (deviceStatus.getBusyTime() > 0) {
                                    if(callback != null)
                                        callback.onDeviceDelayReceived(deviceStatus.getBusyTime());
                                    Intent intent = new Intent(activity, DelayActivity.class)
                                            .putExtra("time", deviceStatus.getBusyTime())
                                            .putExtra("total_time", deviceStatus.getBusyTotalTime())
                                            .putExtra("from", activity.getClass());

                                    intent.putExtra("status", deviceStatus.getStatus());
                                    intent.putExtra("from_chat", (activity instanceof ChatActivity));

                                    if(!(activity instanceof ChatActivity)) {
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    }

                                    activity.startActivityForResult(intent, 0);
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                error.printStackTrace();
                            }
                        });
    }
}
