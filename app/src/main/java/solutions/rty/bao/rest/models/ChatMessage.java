// Copyright (c) 2015. RTY Solutions. All rights reserved

package solutions.rty.bao.rest.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable(tableName = "tb_chat_message")
public class ChatMessage {

    public final static String SEND_AT = "send_at";

    @DatabaseField(id = true)
    private int id;

    @DatabaseField
    private String sender;

    @DatabaseField
    private String receiver;

    @DatabaseField(columnName = SEND_AT)
    @JsonProperty(value = "send_at")
    private Date sendAt;

    @DatabaseField
    private String message;
    @DatabaseField(foreign = true) private Session session;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public Date getSendAt() {
        return sendAt;
    }

    public void setSendAt(Date sendAt) {
        this.sendAt = sendAt;
    }
}
