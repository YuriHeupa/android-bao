package solutions.rty.bao.rest.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Yuri Heupa on 04/05/15.
 * <p/>
 * Copyright (C) 2015 RTY Solutions - All Rights Reserved
 */
public class DeviceStatus {

    String status;

    @JsonProperty(value = "busy_time")
    float busyTime;

    @JsonProperty(value = "match_delay_time")
    float busyTotalTime;

    public DeviceStatus() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getBusyTime() {
        return busyTime;
    }

    public void setBusyTime(float busyTime) {
        this.busyTime = busyTime;
    }

    public float getBusyTotalTime() {
        return busyTotalTime;
    }

    public void setBusyTotalTime(float busyTotalTime) {
        this.busyTotalTime = busyTotalTime;
    }
}
