package solutions.rty.bao.rest;

import com.fasterxml.jackson.databind.ObjectMapper;

import retrofit.RestAdapter;
import retrofit.converter.JacksonConverter;
import solutions.rty.bao.rest.services.ApiService;
import solutions.rty.bao.rest.services.ChatService;

public class BaoClient {


    private static final String BASE_URL = "http://bao.sandbox.rtydev.com.br";
//    public static final String BASE_URL = "http://7d1c31d6.ngrok.com";


    private static BaoClient mInstance;
    private RestAdapter restAdapter;
    private final ApiService apiService;
    private final ChatService chatService;

    public static synchronized BaoClient getInstance() {
        if(mInstance == null)
            mInstance = new BaoClient();
        return mInstance;
    }

    private BaoClient() {
        restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .setEndpoint(BASE_URL)
                .setConverter(new JacksonConverter(new ObjectMapper()))
                .build();
        apiService = restAdapter.create(ApiService.class);
        chatService = restAdapter.create(ChatService.class);
    }

//    private static RestAdapter.Builder getAdapterBuilder() {
//        return new RestAdapter.Builder()
//                .setLogLevel(RestAdapter.LogLevel.BASIC)
//                .setEndpoint(BASE_URL)
//                .setConverter(new JacksonConverter(new ObjectMapper()));
//
//    }

//    private static <T> T createService(Class<T> serviceClass, final String token) {
//        RestAdapter.Builder builder = getAdapterBuilder();
//
//        if (token != null) {
//            builder.setRequestInterceptor(new RequestInterceptor() {
//                @Override
//                public void intercept(RequestFacade request) {
//                    request.addHeader("Accept", "application/json");
//                    request.addHeader("Authorization", "Token "+token);
//                }
//            });
//        }
//
//        RestAdapter adapter = builder.build();
//        return adapter.create(serviceClass);
//    }

    public static ApiService getApiService() {
        return getInstance().apiService;
    }

    public static ChatService getChatService() {
        return getInstance().chatService;
    }



}