package solutions.rty.bao.rest.services;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Query;
import solutions.rty.bao.rest.models.ChatMessage;

/**
 * Created by Yuri Heupa on 28/04/15.
 * <p/>
 * Copyright (C) 2015 RTY Solutions - All Rights Reserved
 */
public interface ChatService {

    @POST("/chat/messages/send")
    void sendMessage(
            @Header("Authorization") String token,
            @Query("to") int receiverId,
            @Body ChatMessage message,
            Callback<ChatMessage> callback);

    @GET("/chat/messages")
    void getMessages(
            @Header("Authorization") String token,
            @Query("session") int session,
            Callback<List<ChatMessage>> callback);

}
