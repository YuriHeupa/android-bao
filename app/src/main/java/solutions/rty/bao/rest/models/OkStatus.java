package solutions.rty.bao.rest.models;

/**
 * Created by Yuri Heupa on 04/05/15.
 * <p/>
 * Copyright (C) 2015 RTY Solutions - All Rights Reserved
 */
public class OkStatus {

    String status;

    public OkStatus() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
