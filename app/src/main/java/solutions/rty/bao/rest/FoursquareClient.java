package solutions.rty.bao.rest;

import android.location.Location;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.JacksonConverter;
import solutions.rty.bao.rest.models.foursquare.FoursquareDto;
import solutions.rty.bao.rest.models.foursquare.Venue;
import solutions.rty.bao.rest.services.FoursquareService;

public class FoursquareClient {

    /**
     * Change this!!! Generate yours at https://foursquare.com/developers/register
     */
    public static final String CLIENT_ID = "MFPKLY0GCCR3FN5NNJWLTGRYQKXRJNLBKPZQVIF30LQFW3IL";
    public static final String CLIENT_SECRET = "PWXWEHCYBZZ4AKGD4ATQB3TR1MQL4NGOJSJTID0S44NC3LCV";
    public static final long API_DATE_VERSION = 20150101;

    private static final String BASE_URL = "https://api.foursquare.com/v2";

    private VenuesCriteriaIntent intent = VenuesCriteriaIntent.CHECKIN;

    private RestAdapter restAdapter;
    private FoursquareService foursquareService;

    public FoursquareClient() {
        restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .setEndpoint(BASE_URL)
                .setConverter(new JacksonConverter(new ObjectMapper()))
                .build();
        foursquareService = restAdapter.create(FoursquareService.class);
    }


    /**
     * Request a list of venues near the current location, optionally matching a search term.
     * @param callback The callback with the request result
     * @param location User's location. (Required for query searches). Optional if using intent=global
     * @param quantity Number of results to return, up to 50
     * @param radius Limit results to venues within this many meters of the specified location. Defaults
     *               to a city-wide area. Only valid for requests with intent=browse, or requests with
     *               intent=checkin and categoryId or query. Does not apply to match intent requests.
     *               The maximum supported radius is currently 100,000 meters.
     * @param categories A string array of categories to limit results to. If you specify this specifying a
     *                   radius may improve results. If specifying a top-level category, all sub-categories
     *                   will also match the query. Does not apply to match intent requests.
     */
    public void searchForVenues(final Callback<List<Venue>> callback, Location location, int quantity, int radius, String... categories) {

        Callback<FoursquareDto> dtoResponse = new Callback<FoursquareDto>() {
            @Override
            public void success(FoursquareDto foursquareDto, Response response) {
                callback.success(foursquareDto.getResponse().getVenues(), response);
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure(error);
            }
        };

        String ll = String.format(Locale.US, "%.6f,%.6f", location.getLatitude(), location.getLongitude());
        System.out.println(ll);
        foursquareService.searchForVenues(
                CLIENT_ID,
                CLIENT_SECRET,
                API_DATE_VERSION,
                ll,
                location.getAccuracy(),
                null,
                Math.min(quantity, 50),
                intent.getValue(),
                Math.min(radius, 100000),
                categories,
                dtoResponse);
    }


    public enum VenuesCriteriaIntent {
        BROWSE("browse"), CHECKIN("checkin"), MATCH("match");

        private final String value;

        private VenuesCriteriaIntent(String string) {
            value = string;
        }

        public String getValue() {
            return value;
        }
    }


}