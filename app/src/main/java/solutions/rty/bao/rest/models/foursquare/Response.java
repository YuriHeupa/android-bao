package solutions.rty.bao.rest.models.foursquare;

import java.util.List;

public class Response {

    public boolean confident;

	private List<Venue> venues;

	public List<Venue> getVenues() {
        return venues;
	}

	public void setVenues(List<Venue> venues) {
		this.venues = venues;
	}
}