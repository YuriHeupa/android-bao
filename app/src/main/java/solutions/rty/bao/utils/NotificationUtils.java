package solutions.rty.bao.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import solutions.rty.bao.R;
import solutions.rty.bao.ui.activity.GuestsActivity;
import solutions.rty.bao.ui.activity.ChatActivity;

/**
 * Created by Yuri Heupa on 14/04/15.
 * <p/>
 * Copyright (C) 2015 RTY Solutions - All Rights Reserved
 */
public class NotificationUtils {

    public static final int CHAT_NOTIFICATION_ID = 1;
    public static final int MATCH_NOTIFICATION_ID = 2;
    public static final int DELAY_NOTIFICATION_ID = 3;

    public static NotificationManager getManager(Context context) {
        return (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public static void clearNotificationsById(Context context, int id) {
        getManager(context).cancel(id);
    }

    public static void showChatNotification(Context context) {

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, ChatActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP),
                PendingIntent.FLAG_CANCEL_CURRENT);

        String msg = "Voce recebeu uma mensagem!";

//        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Notification.Builder mBuilder =
                new Notification.Builder(context)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(context.getString(R.string.app_name))
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setStyle(new Notification.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg)
                        .setDefaults(NotificationCompat.DEFAULT_ALL);

//        mBuilder.setVibrate(new long[] { 500, 200, 500, 200, 500, 200, 500, 200});

//        mBuilder.setVibrate(new long[] { 500, 200, 500, 200, 500, 200, 500, 200});

//        //LED
//        mBuilder.setLights(Color.RED, 3000, 3000);
//
//        String soundUri = "android.resource://"
//                + getPackageName() + "/" + R.raw.funk;
////        Ton
//        mBuilder.setSound(Uri.parse(soundUri));

        mBuilder.setContentIntent(contentIntent);
        getManager(context).notify(CHAT_NOTIFICATION_ID, mBuilder.build());
    }

    public static void showMatchNotification(Context context) {

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, GuestsActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP),
                PendingIntent.FLAG_CANCEL_CURRENT);

        String msg = "Voce Recebeu um match!";

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(context.getString(R.string.app_name))
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        getManager(context).notify(MATCH_NOTIFICATION_ID, mBuilder.build());
    }
    public static void showDelayNotification(Context context) {

//        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
//                new Intent(this, MainActivity.class), 0);

        String msg = "Seu tempo de delay acabou, voce pode voltar a usar o app!";

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(context.getString(R.string.app_name))
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setCategory(NotificationCompat.CATEGORY_STATUS)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        getManager(context).notify(DELAY_NOTIFICATION_ID, mBuilder.build());
    }
}
