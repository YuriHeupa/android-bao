package solutions.rty.bao.utils;


import org.apache.http.Header;

/**
 * Created by Yuri Heupa on 08/01/15.
 * Copyright (C) 2013 - All Rights Reserved
 */
public class HttpUtils {

    private static final int IO_BUFFER_SIZE = 4 * 1024;


    public static String formatFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
        StringBuilder builder = new StringBuilder();
        builder.append("Request finished with failure\n")
                .append("(Status: ").append(String.valueOf(statusCode));
        if(responseString != null)
            builder.append(" Response: ").append(responseString);
        if(throwable != null)
            builder.append(" Exception: ").append(throwable.getMessage());
        builder.append(")");
        return builder.toString();
    }

    public static String formatSuccess(int statusCode, Header[] headers, String responseString) {
        StringBuilder builder = new StringBuilder();
        builder.append("Request finished with success\n")
        .append("(Status: ").append(String.valueOf(statusCode));
        if(responseString != null)
            builder.append(" Response: ").append(responseString);
        builder.append(")");
        return builder.toString();
    }
}
