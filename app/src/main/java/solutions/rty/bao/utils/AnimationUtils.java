package solutions.rty.bao.utils;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.view.View;
import android.view.animation.CycleInterpolator;

/**
 * Created by Yuri Heupa on 18/12/14.
 * Copyright (C) 2013 - All Rights Reserved
 */
public class AnimationUtils {

    public static void shake(View v) {
        PropertyValuesHolder animationAlphaBounce = PropertyValuesHolder.ofFloat("translationX", 0f, 10f);
        ObjectAnimator animBounce = ObjectAnimator.ofPropertyValuesHolder(v, animationAlphaBounce);
        animBounce.setInterpolator(new CycleInterpolator(5f));
        animBounce.setDuration(500);
        animBounce.start();
    }
}
