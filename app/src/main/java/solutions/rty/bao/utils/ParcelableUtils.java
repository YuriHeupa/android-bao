package solutions.rty.bao.utils;


import android.content.Entity;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Yuri Heupa on 30/01/15.
 * Copyright (C) 2013 - All Rights Reserved
 */
public class ParcelableUtils {

    public static <T extends Entity> void parcelCollection(final Parcel out, final Collection<T> collection) {
        if (collection != null) {
            out.writeInt(collection.size());
            out.writeSerializable(new ArrayList<>(collection));
        } else {
            out.writeInt(-1);
        }
    }

    public static <T extends Entity> Collection<T> unparcelCollection(final Parcel in, final Parcelable.Creator<T> creator) {
        final int size = in.readInt();

        if (size >= 0) {
            final List<T> list = new ArrayList<>(size);
            in.readTypedList(list, creator);
            return list;
        } else {
            return null;
        }
    }

}
