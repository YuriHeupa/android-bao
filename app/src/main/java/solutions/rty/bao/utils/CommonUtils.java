package solutions.rty.bao.utils;

import java.util.Random;

/**
 * Created by Yuri Heupa on 23/01/15.
 * Copyright (C) 2013 - All Rights Reserved
 */
public class CommonUtils {

    public static int getRandomInRange(int min, int max) {
        Random rand = new Random();
        return rand.nextInt(max-min+1) + min;
    }

    public static float getProgress(int value, int min, int max) {
        if (min == max) {
            throw new IllegalArgumentException("Max (" + max + ") cannot equal min (" + min + ")");
        }

        return (value - min) / (float) (max - min);
    }

}
