package solutions.rty.bao.utils;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;

import java.util.Random;

/**
 * Created by Yuri Heupa on 22/01/15.
 * Copyright (C) 2013 - All Rights Reserved
 */
public class UIUtils {

    private static final int RES_IDS_ACTION_BAR_SIZE[] = {
            0x10102eb
    };

    public static int getRandomColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

//    public static void startActivityWithTransition(ActionBarActivity fromActivity, Intent intent, final View clickedView,
//                                                   final String transitionName) {
//
//        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
//                fromActivity,
//                clickedView,
//                transitionName);
//        ActivityCompat.startActivity(fromActivity, intent,
//                options.toBundle());
//    }
//
//    public static void startActivityWithTransition(ActionBarActivity fromActivity,
//                                                   Intent intent,
//                                                   Pair<View, String>... sharedElements) {
//
//        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
//                fromActivity,
//                sharedElements);
//        ActivityCompat.startActivity(fromActivity, intent,
//                options.toBundle());
//    }

    public static int calculateActionBarSize(Context context) {
        Resources.Theme theme;
        TypedArray typedarray;
        if (context != null)
        {
            if ((theme = context.getTheme()) != null && (typedarray = theme.obtainStyledAttributes(RES_IDS_ACTION_BAR_SIZE)) != null)
            {
                float f = typedarray.getDimension(0, 0.0F);
                typedarray.recycle();
                return (int)f;
            }
        }
        return 0;
    }

    public static void setSelectableItemBackground(View view) {
        int pL = view.getPaddingLeft();
        int pT = view.getPaddingTop();
        int pR = view.getPaddingRight();
        int pB = view.getPaddingBottom();
        TypedValue outValue = new TypedValue();
        view.getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground,
                outValue, true);
        view.setBackgroundResource(outValue.resourceId);
        view.setPadding(pL, pT, pR, pB);
    }


}
