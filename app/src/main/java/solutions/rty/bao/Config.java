package solutions.rty.bao;

import android.content.Context;
import android.net.Uri;

import solutions.rty.bao.rest.models.Guest;
import solutions.rty.bao.rest.models.Session;

/**
 * Created by Yuri Heupa on 15/12/14.
 * Copyright (C) 2013 - All Rights Reserved
 */
public class Config {

    private static String SERVER_AUTHORITY;
    private static String SERVER_SCHEME;

    public static void init(Context context) {
        SERVER_AUTHORITY = context.getString(R.string.server_authority);
        SERVER_SCHEME = context.getString(R.string.server_scheme);
    }

    static public final String API_PATH = "api";

    static public final String GCM_SENDER_ID = "854455871524";

    public static String getWebsocketUrl(Session session) {
        return "ws://" + SERVER_AUTHORITY + "/ws/" + session.getDeviceId() + "?subscribe-broadcast";
    }

    public static Uri.Builder defaultApiRouter() {
        return new Uri.Builder()
                .scheme(SERVER_SCHEME)
                .authority(SERVER_AUTHORITY)
                .appendPath(API_PATH);
    }

    public static Uri.Builder chatRouter() {
        return new Uri.Builder()
                .scheme(SERVER_SCHEME)
                .authority(SERVER_AUTHORITY)
                .appendPath("chat");
    }

    private static Uri.Builder deviceRouter() {
        return defaultApiRouter()
                .appendPath("device");
    }

    private static Uri.Builder sessionRouter() {
        return defaultApiRouter()
                .appendPath("session");
    }

    public static String getRegisterUrl(Session session) {
        Uri.Builder builder = deviceRouter()
                .appendPath("register")
                .appendQueryParameter("place", session.getPlaceId())
                .appendQueryParameter("gender", session.getMyGender())
                .appendQueryParameter("id", session.getDeviceId());

        if(session.getDevicePushId() != null)
            builder.appendQueryParameter("push", "android " + session.getDevicePushId());

        return builder.build().toString();
    }


    public static String getDeviceStatus(String deviceId) {
        return deviceRouter()
                .appendPath("status")
                .appendQueryParameter("id", deviceId)
                .build()
                .toString();
    }

    public static String getConfirmMatchUrl(int id) {
        return defaultApiRouter()
                .appendEncodedPath("match/confirm")
                .appendEncodedPath(String.valueOf(id))
                .build()
                .toString();
    }

    public static String getContactsUrl(String placeId, String gender) {
        return sessionRouter()
                .appendPath("contacts")
                .appendQueryParameter("place", placeId)
                .appendQueryParameter("gender", gender)
                .build()
                .toString();

    }

    public static String getHighlightUrl(Guest guest) {
        return sessionRouter()
                .appendEncodedPath(String.valueOf(guest.getId()))
                .appendEncodedPath(guest.isHighlighted() ? "unhighlight" : "highlight")
                .build()
                .toString();
    }

    public static String getSendMessageUrl(int receiverId) {
        return chatRouter()
                .appendPath("messages")
                .appendPath("send")
                .appendQueryParameter("to", String.valueOf(receiverId))
                .build()
                .toString();
    }
}
