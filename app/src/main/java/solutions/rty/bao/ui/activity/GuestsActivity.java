package solutions.rty.bao.ui.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import solutions.rty.bao.BaoApplication;
import solutions.rty.bao.R;
import solutions.rty.bao.data.SessionManager;
import solutions.rty.bao.rest.models.Guest;
import solutions.rty.bao.ui.adapter.GuestsAdapter;
import solutions.rty.bao.ui.presenter.GuestsPresenter;
import solutions.rty.bao.ui.widget.SpacesItemDecoration;
import solutions.rty.bao.utils.NotificationUtils;
import solutions.rty.picasso.CircleTransform;

import static solutions.rty.bao.data.SessionManager.getActiveSession;

public class GuestsActivity extends AppCompatActivity implements GuestsPresenter.GuestsView {

    private static final String TAG = GuestsActivity.class.getSimpleName();

    @InjectView(R.id.toolbar)
    Toolbar mToolbar;
    @InjectView(R.id.swipe_refresh)
    SwipeRefreshLayout mSwipeRefresh;

    @InjectView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    GuestsAdapter mAdapter;

    GuestsPresenter presenter;

    MenuItem filterHighlited;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guests);
        ButterKnife.inject(this);

        this.setSupportActionBar(mToolbar);
        setTitle("");


        presenter = new GuestsPresenter(this, BaoApplication.getBus());

        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.requestGuests();
            }
        });

        mSwipeRefresh.setColorScheme(R.color.colorAccent,
                R.color.colorPrimaryDark,
                R.color.colorAccentLight,
                R.color.colorPrimary);


        AnimationSet set = new AnimationSet(true);

        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(200);
        set.addAnimation(animation);

        animation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f
        );
        animation.setDuration(400);

        set.addAnimation(animation);


        LayoutAnimationController controller = new LayoutAnimationController(set, 0.2f);
        mRecyclerView.setLayoutAnimation(controller);
        mRecyclerView.setAnimationCacheEnabled(true);
        float spacing = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getResources().getDisplayMetrics());
        mRecyclerView.addItemDecoration(new SpacesItemDecoration(spacing));

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new GuestsAdapter(this);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.startLayoutAnimation();
    }

    @Override
    public void showReadyDialog(final int guestId, final String targetPicture) {
        final LinearLayout layout = (LinearLayout) LayoutInflater.from(GuestsActivity.this).inflate(R.layout.dialog_ready, null);
        ImageView imageView = (ImageView) layout.findViewById(R.id.dialog_image);
        Picasso.with(GuestsActivity.this)
                .load(targetPicture)
                .transform(new CircleTransform())
                .into(imageView);
        AlertDialog.Builder builder = new AlertDialog.Builder(GuestsActivity.this).setView(layout).setCancelable(false);

        final AlertDialog alert = builder.create();
        alert.show();
        Button okButton = (Button) layout.findViewById(R.id.ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                startActivity(new Intent(GuestsActivity.this, ChatActivity.class)
                        .putExtra("id", guestId).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                getActiveSession().setLastChatPicture(targetPicture);
                SessionManager.saveSession();
                finish();
            }
        });
    }

    @Override
    public void setGuests(List<Guest> guests) {
        mAdapter.setGuests(guests);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void setRefreshing(boolean refreshing) {
        mSwipeRefresh.setRefreshing(refreshing);
    }

    @Override
    public void showMessagePanel(String message, Style style) {
        if (infiniteCrouton != null) {
            Crouton.hide(infiniteCrouton);
            infiniteCrouton = null;
        }
        Crouton.makeText(this, message, style, R.id.browse_container).show();
    }

    Crouton infiniteCrouton;

    @Override
    public void showInfiniteMessagePanel(String message, Style style) {
        if (infiniteCrouton != null) {
            Crouton.hide(infiniteCrouton);
            infiniteCrouton = null;
        }
        infiniteCrouton = Crouton.makeText(this, message, style, R.id.browse_container)
                .setConfiguration(new Configuration.Builder().setDuration(Configuration.DURATION_INFINITE).build());
        infiniteCrouton.show();
    }

    @Override
    public void showMessageToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean isFilteringHighlited() {
        return filterHighlited.isChecked();
    }

    @Override
    public void showMatchNotification() {
        NotificationUtils.showMatchNotification(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
        NotificationUtils.clearNotificationsById(this, NotificationUtils.MATCH_NOTIFICATION_ID);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        Crouton.clearCroutonsForActivity(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.browse_filters, menu);
        filterHighlited = menu.findItem(R.id.menu_filter_highlited);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        String targetGender = getActiveSession().getGenderWanted();
        MenuItem filterFemale = menu.findItem(R.id.menu_filter_female);
        MenuItem filterMale = menu.findItem(R.id.menu_filter_male);
//        MenuItem filterHighlited = menu.findItem(R.id.menu_filter_highlited);
//        filterFemale.setIcon(targetGender.equals("female") ? R.drawable.menu_filter_female_on : R.drawable.menu_filter_female_off);
//        filterMale.setIcon(targetGender.equals("male") ? R.drawable.menu_filter_male_on : R.drawable.menu_filter_male_off);
//        filterHighlited.setIcon(filteringHighlited ? R.drawable.menu_filter_highlighted_on : R.drawable.menu_filter_highlighted_off);
//        TODO TEMPORARIO, PROVAVELMENTE VAI VOLTAR
        menu.removeItem(R.id.menu_filter_female);
        menu.removeItem(R.id.menu_filter_male);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
//            case R.id.menu_filter_female:
//                getActiveSession().setGenderWanted("female");
//                presenter.refreshGuests();
//                downloadContacts();
//                invalidateOptionsMenu();
//                return true;
//            case R.id.menu_filter_male:
//                getActiveSession().setGenderWanted("male");
//                presenter.refreshGuests();
//                downloadContacts();
//                invalidateOptionsMenu();
//                return true;
            case R.id.menu_filter_highlited:
                item.setChecked(!item.isChecked());
                item.setIcon(item.isChecked() ? R.drawable.menu_filter_highlighted_on : R.drawable.menu_filter_highlighted_off);
                presenter.refreshGuests();
//                invalidateOptionsMenu();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


}
