package solutions.rty.bao.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import solutions.rty.bao.R;
import solutions.rty.bao.data.SessionManager;
import solutions.rty.bao.rest.models.foursquare.Venue;
import solutions.rty.bao.ui.activity.PlaceRegisterActivity;

public class VenuesAdapter extends RecyclerView.Adapter<VenuesAdapter.EventViewHolder> {

    private final Context mContext;

    private List<Venue> mVenues = new ArrayList<>();

    public VenuesAdapter(Context context) {
        this.mContext = context;
    }

    public void setVenues(List<Venue> events) {
        this.mVenues = events;
        notifyDataSetChanged();
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_item_place, parent, false);
        return new EventViewHolder(v);
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        final Venue venue = mVenues.get(position);

        holder.placeName.setText(venue.getName());
        String addressText = venue.getLocation().getAddress();
        if(!TextUtils.isEmpty(venue.getLocation().getPostalCode()))
            addressText += " CEP: " + venue.getLocation().getPostalCode();
        holder.placeAddress.setText(addressText);
    }

    @Override
    public int getItemCount() {
        return mVenues.size();
    }

    public class EventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @InjectView(R.id.place_name)
        TextView placeName;
        @InjectView(R.id.place_address) TextView placeAddress;


        public EventViewHolder(final View v) {
            super(v);
            ButterKnife.inject(this, v);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final String placeId = mVenues.get(getAdapterPosition()).getId();
            SessionManager.getActiveSession().setPlaceId(placeId);
            mContext.startActivity(new Intent(mContext, PlaceRegisterActivity.class));
        }
    }

}