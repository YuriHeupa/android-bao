package solutions.rty.bao.ui.adapter;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import solutions.rty.bao.R;
import solutions.rty.bao.data.SessionManager;
import solutions.rty.bao.rest.BaoClient;
import solutions.rty.bao.rest.models.Guest;
import solutions.rty.bao.rest.models.OkStatus;

public class GuestsAdapter extends RecyclerView.Adapter<GuestsAdapter.ViewHolder> {

    private Context context;

    private List<Guest> guests = new ArrayList<>();

    public GuestsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.list_item_contact, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Guest guest = guests.get(position);
        holder.mFavoriteContact.setImageResource(guest.isHighlighted() ? R.drawable.button_like_checked : R.drawable.button_like_normal);
        Picasso.with(context)
                .load(guest.getImage())
                .placeholder(android.R.color.darker_gray)
                .into(holder.mContactPhoto);
    }

    public void setGuests(List<Guest> guests) {
        this.guests = guests;
    }

    @Override
    public int getItemCount() {
        return guests.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.contact_photo)
        ImageView mContactPhoto;
        @InjectView(R.id.favorite_contact)
        ImageButton mFavoriteContact;
        @InjectView(R.id.contact_photo_container)
        FrameLayout mContactPhotoContainer;

        public ViewHolder(final View root) {
            super(root);
            ButterKnife.inject(this, root);
//                mContactPhotoContainer.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        getCallback().onFragmentItemClick();
//                    }
//                });
            mFavoriteContact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Guest guest = guests.get(getAdapterPosition());
                    if (!guest.isHighlighted()) {
                        mFavoriteContact.setImageResource(R.drawable.highlight_anim);
                    } else {
                        mFavoriteContact.setImageResource(R.drawable.unhighlight_anim);
                    }
                    AnimationDrawable buttonAnimation = (AnimationDrawable) mFavoriteContact.getDrawable();
                    buttonAnimation.start();

                    mFavoriteContact.setEnabled(false);
                    BaoClient.getApiService()
                            .highlightContact("Token " + SessionManager.getActiveSession().getToken(),
                                    guest.getId(), guest.isHighlighted() ? "unhighlight" : "highlight",
                                    new Callback<OkStatus>() {
                                        @Override
                                        public void success(OkStatus okStatus, Response response) {
                                            guest.setHighlighted(!guest.isHighlighted());
                                            mFavoriteContact.setImageResource(guest.isHighlighted() ? R.drawable.button_like_checked : R.drawable.button_like_normal);
                                            mFavoriteContact.setEnabled(true);
                                        }

                                        @Override
                                        public void failure(RetrofitError error) {
                                            error.printStackTrace();
                                            Toast.makeText(context,
                                                    "Não foi possível fazer a requisição, verifique sua conexão com a internet.",
                                                    Toast.LENGTH_SHORT)
                                                    .show();
                                            mFavoriteContact.setImageResource(guest.isHighlighted() ? R.drawable.button_like_checked : R.drawable.button_like_normal);
                                            mFavoriteContact.setEnabled(true);
                                        }
                                    });
                }
            });
        }
    }
}