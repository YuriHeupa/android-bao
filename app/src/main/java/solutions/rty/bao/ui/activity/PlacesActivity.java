package solutions.rty.bao.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import solutions.rty.bao.BaoApplication;
import solutions.rty.bao.R;
import solutions.rty.bao.data.SessionManager;
import solutions.rty.bao.receiver.GCMHandler;
import solutions.rty.bao.rest.DeviceDelayRequest;
import solutions.rty.bao.rest.models.foursquare.Venue;
import solutions.rty.bao.ui.adapter.VenuesAdapter;
import solutions.rty.bao.ui.presenter.PlacesPresenter;
import solutions.rty.bao.utils.GPlayServicesUtils;

public class PlacesActivity extends AppCompatActivity
        implements
        OnMapReadyCallback, PlacesPresenter.PlacesView {
    /**
     * Tag used on log messages.
     */
    static final String TAG = PlacesActivity.class.getSimpleName();

    private static final int SPLASH_TIME_OUT = 3000;
    @InjectView(R.id.logo)
    FrameLayout mLogo;
    @InjectView(R.id.logo_text)
    ImageView mLogoText;
    @InjectView(R.id.swipe_refresh)
    SwipeRefreshLayout mSwipeRefresh;
    @InjectView(R.id.place_list)
    RecyclerView mPlaceList;
    @InjectView(R.id.image_expand)
    ImageView mImageExpand;

    @InjectView(R.id.sliding_layout)
    SlidingUpPanelLayout slidingLayout;

//    @InjectView(R.id.space_offset)
//    Space spaceOffset;

    VenuesAdapter mAdapter;

    GoogleMap mMap;

    PlacesPresenter presenter;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);
        ButterKnife.inject(this);

        mPlaceList.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mPlaceList.setLayoutManager(layoutManager);
        mAdapter = new VenuesAdapter(this);
        mPlaceList.setAdapter(mAdapter);
        // Check device for Play Services APK. If check succeeds, proceed with
        //  GCM registration.®
        if (GPlayServicesUtils.checkPlayServices(this)) {
            GCMHandler.instantiate(getApplicationContext(), new GCMHandler.OnGCMDeviceRegistered() {
                @Override
                public void onGCMDeviceRegistered(String regId) {
                    SessionManager.getActiveSession().setDevicePushId(regId);
                    SessionManager.saveSession();
                }
            });
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
        animateLogo();

        MapFragment mapFragment =
                (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        presenter = new PlacesPresenter(this, BaoApplication.getBus());

        slidingLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View view, float v) {

            }

            @Override
            public void onPanelCollapsed(View view) {
//                mPlaceList.setClickable(false);
//                System.out.println("collapsed");
                mImageExpand.setImageResource(R.drawable.ic_expand_more);

            }

            @Override
            public void onPanelExpanded(View view) {
//                mPlaceList.setClickable(true);
//                System.out.println("expanded");
                mImageExpand.setImageResource(R.drawable.ic_expand_less);
            }

            @Override
            public void onPanelAnchored(View view) {

            }

            @Override
            public void onPanelHidden(View view) {

            }
        });

        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.requestPlaces();
            }
        });

        mSwipeRefresh.setColorScheme(R.color.colorAccent,
                R.color.colorPrimaryDark,
                R.color.colorAccentLight,
                R.color.colorPrimary);

    }

    private void animateLogo() {
        Animation logoFadeOut = new AlphaAnimation(1, 0);
        logoFadeOut.setInterpolator(new AccelerateInterpolator()); //and this
        logoFadeOut.setStartOffset(SPLASH_TIME_OUT/2);
        logoFadeOut.setDuration(SPLASH_TIME_OUT/6);
        logoFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLogo.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mLogo.setAnimation(logoFadeOut);
        logoFadeOut.start();
        Animation logoTextScaleIn = new ScaleAnimation(10f, 1f, 10f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        logoTextScaleIn.setInterpolator(new DecelerateInterpolator());
        logoTextScaleIn.setDuration(SPLASH_TIME_OUT/8);

        Animation logoTextScaleOut = new ScaleAnimation(1f, 10f, 1f, 10f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        logoTextScaleOut.setInterpolator(new AccelerateInterpolator());
        logoTextScaleOut.setStartOffset(SPLASH_TIME_OUT/2);
        logoTextScaleOut.setDuration(SPLASH_TIME_OUT/8);


        Animation logoTextRotate = new RotateAnimation(-10, 10, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        logoTextRotate.setInterpolator(new AccelerateDecelerateInterpolator());
        logoTextRotate.setDuration(300);
        logoTextRotate.setRepeatMode(Animation.REVERSE);
        logoTextRotate.setRepeatCount(5);
        logoTextRotate.setFillAfter(true);

        Animation logoTextRotate2 = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        logoTextRotate2.setInterpolator(new AccelerateInterpolator());
        logoTextRotate2.setStartOffset(SPLASH_TIME_OUT / 2);
        logoTextRotate2.setRepeatMode(Animation.RESTART);
        logoTextRotate2.setDuration(SPLASH_TIME_OUT / 8);
        logoTextRotate2.setFillAfter(true);

        AnimationSet logoTextAnimationSet = new AnimationSet(false); //change to false
        logoTextAnimationSet.addAnimation(logoTextScaleIn);
        logoTextAnimationSet.addAnimation(logoTextScaleOut);
        logoTextAnimationSet.addAnimation(logoTextRotate);
        logoTextAnimationSet.addAnimation(logoTextRotate2);
        mLogoText.setAnimation(logoTextAnimationSet);
        logoTextAnimationSet.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        DeviceDelayRequest.validate(this);
        presenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onStop();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        map.setMyLocationEnabled(true);
    }

    @Override
    public void setVenues(List<Venue> venues) {
        mAdapter.setVenues(venues);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void setRefreshing(boolean refreshing) {
        mSwipeRefresh.setRefreshing(refreshing);
    }

    @Override
    public void showMessagePanel(String message, Style style) {
        Crouton.makeText(this, message, style).show();
    }

    @Override
    public void moveMapCamera(CameraUpdate cameraUpdate) {
        mMap.moveCamera(cameraUpdate);
    }

    @Override
    public void showMessageToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getContext() {
        return this;
    }
}
