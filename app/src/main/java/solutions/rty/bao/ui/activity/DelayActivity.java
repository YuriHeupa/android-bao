// Copyright (c) 2015. RTY Solutions. All rights reserved

package solutions.rty.bao.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import butterknife.InjectView;
import solutions.rty.bao.R;
import solutions.rty.bao.data.SessionManager;
import solutions.rty.bao.utils.NotificationUtils;
import solutions.rty.bao.ui.widget.HoloCircularProgressBar;
import solutions.rty.picasso.CircleTransform;

public class DelayActivity extends AppCompatActivity {

    private static final String TAG = DelayActivity.class.getSimpleName();

    @InjectView(R.id.delay_time)
    TextView mDelayTimeText;
    @InjectView(R.id.delay_message)
    TextView mDelayMessageView;
    @InjectView(R.id.button_back_chat)
    ImageButton mButtonBackChat;
    @InjectView(R.id.button_back_browse)
    ImageButton mButtonBackBrowse;
    @InjectView(R.id.time_progress)
    HoloCircularProgressBar mTimeProgress;
    @InjectView(R.id.delay_picture)
    ImageView mDelayPicture;

    @InjectView(R.id.buttons_container)
    LinearLayout mButtonsContainer;

    public final static int BACK_TO_BROWSE = 1;

    float delayTime = 0f;
    float totalTime = 0f;
    boolean timeFinished;
    boolean fromChat;
    String status;

    boolean inBackground;

    Class<?> fromClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delay);
        final Bundle extras = getIntent().getExtras();
        if(extras != null) {
            delayTime = extras.getFloat("time");
            totalTime = extras.getFloat("total_time");
            status = extras.getString("status");
            fromChat = extras.getBoolean("from_chat");
            fromClass = (Class<?>) extras.get("from");
//            if (delayTime == 0)
//                finish();
        } else {
            finish();
        }

        ButterKnife.inject(this);

        mDelayPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                LUtils.getInstance(DelayActivity.this)
//                        .startActivityWithTransition(new Intent(DelayActivity.this, PhotoExpandedActivity.class)
//                                .putExtra("picture", SessionManager.getActiveSession().getLastChatPicture()), v, "circular_reveal");
                startActivity(new Intent(DelayActivity.this, PhotoExpandedActivity.class)
                        .putExtra("picture", SessionManager.getActiveSession().getLastChatPicture()));
            }
        });

        Picasso.with(this)
                .load(SessionManager.getActiveSession().getLastChatPicture())
                .transform(new CircleTransform())
                .into(mDelayPicture);

        if(!fromChat) {
            mButtonsContainer.setVisibility(View.GONE);
        }

        mButtonBackChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mButtonBackBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(BACK_TO_BROWSE);
                finish();
            }
        });
        mButtonBackBrowse.setEnabled(false);

//        if(status != null && !status.isEmpty())
//            mDelayMessageView.setText(status);
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        mTimeProgress.startAnimation(animation);

        new CountDownTimer((long) delayTime, 1000) {

            public void onTick(long millisUntilFinished) {
                float progress = millisUntilFinished/totalTime;
                String timeText = String.format("%02d:%02d:%02d",
                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))
                );

                mDelayTimeText.setText(getDurationBreakdown(millisUntilFinished));
                mTimeProgress.setProgress(progress);
            }

            public void onFinish() {
                mTimeProgress.setProgress(0);
                mDelayTimeText.setText("Tempo acabou!");
                mTimeProgress.clearAnimation();
                if(inBackground) {
                    NotificationUtils.showDelayNotification(DelayActivity.this);
                } else {
                    if (!fromChat) {
                        finish();
                        startActivity(new Intent(DelayActivity.this, PlacesActivity.class));
                    }
                }
                timeFinished = true;
                mButtonBackBrowse.setEnabled(true);
            }
        }.start();

    }
    /**
     * Convert a millisecond duration to a string format
     *
     * @param millis A duration to convert to a string form
     * @return A string of the form "X Days Y Hours Z Minutes A Seconds".
     */
    public static String getDurationBreakdown(long millis) {
        if(millis < 0)
        {
            throw new IllegalArgumentException("Duration must be greater than zero!");
        }

        long days = TimeUnit.MILLISECONDS.toDays(millis);
        millis -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);

        StringBuilder sb = new StringBuilder(64);
        if(days > 0) {
            sb.append(days);
            sb.append(" Dias ");
        }
        if(hours > 0) {
            sb.append(hours);
            sb.append(" Horas ");
        }
        if(minutes > 0) {
            sb.append(minutes);
            sb.append(" Minutos ");
        }
        sb.append(seconds);
        sb.append(" Segundos");

        return(sb.toString());
    }

    @Override
    public void onBackPressed() {
//        if(!timeFinished) {
//            return;
//        }
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        inBackground = false;
        NotificationUtils.clearNotificationsById(this, NotificationUtils.DELAY_NOTIFICATION_ID);
    }

    @Override
    protected void onPause() {
        super.onPause();
        inBackground = true;
    }
}

