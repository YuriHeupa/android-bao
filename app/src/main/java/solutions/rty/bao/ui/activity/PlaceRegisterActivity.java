package solutions.rty.bao.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import solutions.rty.bao.R;
import solutions.rty.bao.data.SessionManager;
import solutions.rty.bao.rest.models.Session;

import static solutions.rty.bao.rest.BaoClient.getApiService;

public class PlaceRegisterActivity extends AppCompatActivity {
    /**
     * Tag used on log messages.
     */
    static final String TAG = PlaceRegisterActivity.class.getSimpleName();

    @InjectView(R.id.me_female_button)
    RadioButton mMeFemaleButton;
    @InjectView(R.id.me_male_button)
    RadioButton mMeMaleButton;
    @InjectView(R.id.want_female_button)
    RadioButton mWantFemaleButton;
    @InjectView(R.id.want_male_button)
    RadioButton mWantMaleButton;
    @InjectView(R.id.gender_container)
    LinearLayout mGenderContainer;

    @InjectView(R.id.group_gender_me)
    RadioGroup mGroupGenderMe;
    @InjectView(R.id.group_gender_want)
    RadioGroup mGroupGenderWant;
    @InjectView(R.id.want_container)
    LinearLayout mWantContainer;


    @InjectView(R.id.im_text)
    TextView imText;
    @InjectView(R.id.iwant_text)
    TextView iwantText;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_register);

        ButterKnife.inject(this);

        Typeface londonFont = Typeface.createFromAsset(getAssets(), "fonts/LondonMM.ttf");

        imText.setTypeface(londonFont);
        iwantText.setTypeface(londonFont);
        mMeMaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionManager.getActiveSession().setMyGender("male");
                mWantContainer.setVisibility(View.VISIBLE);
                if (mWantFemaleButton.isChecked() || mWantMaleButton.isChecked())
                    dispatchTakePictureIntent();
            }
        });
        mMeFemaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionManager.getActiveSession().setMyGender("female");
                mWantContainer.setVisibility(View.VISIBLE);
                if (mWantFemaleButton.isChecked() || mWantMaleButton.isChecked())
                    dispatchTakePictureIntent();

            }
        });
        mWantMaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionManager.getActiveSession().setGenderWanted("male");
                if(mMeFemaleButton.isChecked() || mMeMaleButton.isChecked())
                    dispatchTakePictureIntent();
            }
        });
        mWantFemaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionManager.getActiveSession().setGenderWanted("female");
                if(mMeFemaleButton.isChecked() || mMeMaleButton.isChecked())
                    dispatchTakePictureIntent();

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) return;
        switch (requestCode) {
            case REQUEST_TAKE_PHOTO:
                setPic();
                break;
        }

    }

    static final int REQUEST_TAKE_PHOTO = 1;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
//                startActivityForResult(Intent.createChooser(takePictureIntent, "Selecione uma opção"), REQUEST_TAKE_PHOTO);
                startActivityForResult(new Intent(PlaceRegisterActivity.this, CameraActivity.class).setData(
                        Uri.fromFile(photoFile)), REQUEST_TAKE_PHOTO);
            }
        }
    }

    String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "BAO_" + timeStamp;
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWantContainer.setVisibility(View.GONE);
        mGroupGenderMe.clearCheck();
        mGroupGenderWant.clearCheck();
    }

    public static Bitmap rotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private void setPic() {

        float maxWidth = 420f;
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
        float scaleFactor = 1.0F;

        if (bitmap.getWidth() > maxWidth)
            scaleFactor = maxWidth / bitmap.getWidth();
        bitmap = Bitmap.createScaledBitmap(bitmap, (int) (bitmap.getWidth() * scaleFactor), (int) (bitmap.getHeight() * scaleFactor), true);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

        try {
            File f = new File(mCurrentPhotoPath);
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            postImage(f);
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }

    }

    ProgressDialog loggingProgressDialog;

    public void postImage(File file) throws FileNotFoundException, URISyntaxException {
        Session session = SessionManager.getActiveSession();

        loggingProgressDialog = new ProgressDialog(PlaceRegisterActivity.this);
        loggingProgressDialog.setIndeterminate(false);
        loggingProgressDialog.setTitle("Autenticando");
        loggingProgressDialog.setMessage("Por favor aguarde...");
        loggingProgressDialog.setCancelable(false);
        loggingProgressDialog.show();

        getApiService().registerSession(session.getDeviceId(),
                session.getMyGender(),
                session.getGenderWanted(),
                session.getPlaceId(),
                session.getDevicePushId(),
                new TypedFile("application/octet-stream", file),
                new Callback<Session>() {
                    @Override
                    public void success(Session s, Response response) {
                        loggingProgressDialog.dismiss();
                        SessionManager.getActiveSession().setToken(s.getToken());
                        SessionManager.getActiveSession().setId(s.getId());
                        SessionManager.saveSession();
                        startActivity(new Intent(PlaceRegisterActivity.this, GuestsActivity.class));
                        finish();

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        error.printStackTrace();
                        loggingProgressDialog.dismiss();

                    }
                });

//        RequestParams params = new RequestParams();
//        params.put("picture", file);
//        RTYUtils.web.POST(params)
//                .url(Config.getRegisterUrl(SessionManager.getActiveSession()))
//                .as(Session.class)
//                .withCallback(new MappedRequestResponse<Session>() {
//                    @Override
//                    public void onStart() {
//                        loggingProgressDialog = new ProgressDialog(SetupActivity.this);
//                        loggingProgressDialog.setIndeterminate(false);
//                        loggingProgressDialog.setTitle("Autenticando");
//                        loggingProgressDialog.setMessage("Por favor aguarde...");
//                        loggingProgressDialog.setCancelable(false);
//                        loggingProgressDialog.show();
//                    }
//
//                    @Override
//                    public void onRequestSuccess(int i, Session s) {
//                        SessionManager.getActiveSession().setToken(s.getToken());
//                        SessionManager.getActiveSession().setId(s.getId());
//                        SessionManager.saveSession();
//                        startActivity(new Intent(SetupActivity.this, BrowseActivity.class));
//                        finish();
//                    }
//
//                    @Override
//                    public void onRequestError(int i, String s, Throwable throwable) {
//                        throwable.printStackTrace();
//                        if (s != null) {
//                            System.out.println(s);
//                        }
//
//                    }
//
//                    @Override
//                    public void onProgress(int bytesWritten, int totalSize) {
//                        loggingProgressDialog.setMax(totalSize);
//                        loggingProgressDialog.setProgress(bytesWritten);
//                    }
//
//                    @Override
//                    public void onFinish() {
//                        loggingProgressDialog.dismiss();
//                    }
//                });
    }
}
