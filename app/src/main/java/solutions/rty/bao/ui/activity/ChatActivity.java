// Copyright (c) 2015. RTY Solutions. All rights reserved

package solutions.rty.bao.ui.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import solutions.rty.bao.BaoApplication;
import solutions.rty.bao.R;
import solutions.rty.bao.data.SessionManager;
import solutions.rty.bao.receiver.WebsocketManager;
import solutions.rty.bao.rest.BaoClient;
import solutions.rty.bao.rest.DeviceDelayRequest;
import solutions.rty.bao.rest.models.ChatMessage;
import solutions.rty.bao.rest.models.OkStatus;
import solutions.rty.bao.ui.adapter.ChatAdapter;
import solutions.rty.bao.utils.NotificationUtils;
import solutions.rty.picasso.CircleTransform;

import static solutions.rty.bao.rest.BaoClient.getApiService;

public class ChatActivity extends AppCompatActivity implements DeviceDelayRequest.DeviceDelayCallback {

    private static final String TAG = ChatActivity.class.getSimpleName();

    @InjectView(R.id.message_list)
    RecyclerView mMessagesList;
    @InjectView(R.id.chat_container)
    LinearLayout mChatContainer;
    @InjectView(R.id.root_view)
    RelativeLayout mRootView;
    @InjectView(R.id.contact_message)
    EditText mMessage;
    @InjectView(R.id.send_message_button)
    Button mSendMessageButton;
    @InjectView(R.id.message_counter)
    TextView mMessageCounterView;
    @InjectView(R.id.chat_picture)
    ImageView mChatPicture;

    private ChatAdapter mAdapter;

    private int receiverId = 0;

    private int messageCounter = 0;

    boolean inBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        final Bundle extras = getIntent().getExtras();
        if(extras != null) {
            receiverId = extras.getInt("id");
            if (receiverId == 0)
                finish();
        } else {
            finish();
        }

        downloadMessages();

        ButterKnife.inject(this);

        final int mHeaderHeight = (int) getResources().getDimension(R.dimen.chat_header);

        mMessagesList.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        mMessagesList.setLayoutManager(layoutManager);
        mAdapter = new ChatAdapter(this, new ArrayList<ChatMessage>());
        mMessagesList.setAdapter(mAdapter);
        mMessagesList.addOnScrollListener(new HidingScrollListener(mHeaderHeight) {
            @Override
            public void onMoved(int distance) {
                mChatPicture.setTranslationY(-distance);
            }

            @Override
            public void onShow() {
                mChatPicture.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();

            }

            @Override
            public void onHide() {
                mChatPicture.animate().translationY(-mHeaderHeight).setInterpolator(new AccelerateInterpolator(2)).start();

            }
        });

        mChatPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChatActivity.this, PhotoExpandedActivity.class)
                        .putExtra("picture", SessionManager.getActiveSession().getLastChatPicture()));
            }
        });

        Picasso.with(this)
                .load(SessionManager.getActiveSession().getLastChatPicture())
                .transform(new CircleTransform())
                .into(mChatPicture);
        BaoApplication.getBus().register(this);

        confirmMatch(receiverId);

//        mMessagesList.setOnScrollListener(new AbsListView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//            }
//
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                final int currentFirstVisibleItem = mMessagesList.getFirstVisiblePosition();
//
//                if (currentFirstVisibleItem > mLastFirstVisibleItem) {
//                    mChatPicture.animate().translationY(-mChatPicture.getBottom()).setInterpolator(new AccelerateInterpolator()).start();
//                } else if (currentFirstVisibleItem < mLastFirstVisibleItem) {
//                    mChatPicture.animate().translationY(0).setInterpolator(new DecelerateInterpolator()).start();
//                }
//                mLastFirstVisibleItem = firstVisibleItem;
//
//            }
//        });


//        mMessagesList.setOnScrollListener(new AbsListView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//            }
//
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                View v = mMessagesList.getChildAt(0);
//                int scrollY = (v == null) ? 0 : (v.getTop() - mMessagesList.getPaddingTop());
//                System.out.println("scrollY " + scrollY);
//
////                mToolbarOffset = Math.max(Math.min(scrollY, 0), mToolbarHeight);
//                System.out.println(Math.max(scrollY, -mToolbarHeight));
//                mChatPicture.setTranslationY(Math.max(Math.min(scrollY, 0), mToolbarHeight));
////                if((mToolbarOffset < mToolbarHeight && scrollY > 0) || (mToolbarOffset > 0 && scrollY < 0)) {
////                    mToolbarOffset += scrollY;
////                }
//            }
//        });

    }

    private void confirmMatch(final int id) {
        getApiService().confirmMatch("Token " + SessionManager.getActiveSession().getToken(),
                id,
                new Callback<OkStatus>() {
                    @Override
                    public void success(OkStatus okStatus, Response response) {
                        // TODO ESPERANDO RECEBER O READY, DEVE TRAVAR A TELA COM UM LOADING
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        error.printStackTrace();
                    }
                });

    }


    @Override
    protected void onResume() {
        super.onResume();
        NotificationUtils.clearNotificationsById(this, NotificationUtils.CHAT_NOTIFICATION_ID);
        inBackground = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        inBackground = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BaoApplication.getBus().unregister(this);
    }


    @OnClick(R.id.send_message_button)
    public void sendMessage() {
        String text = mMessage.getText().toString();
        if(messageCounter >= 5) {
            Toast.makeText(ChatActivity.this,
                    "Você atingiu o limite de mensagens!",
                    Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        if(text == null || text.isEmpty())
            return;

        ChatMessage messageBody = new ChatMessage();
        messageBody.setMessage(text);

        mMessage.setText("");
        mSendMessageButton.setEnabled(false);

        BaoClient.getChatService()
                .sendMessage("Token " + SessionManager.getActiveSession().getToken(), receiverId,
                        messageBody,
                        new Callback<ChatMessage>() {
                            @Override
                            public void success(ChatMessage chatMessage, Response response) {
                                appendMessage(chatMessage);
                                messageCounter += 1;
                                mMessageCounterView.setText(messageCounter + "/5");
                                if(messageCounter >= 5) {
                                    removeChatCommand();
                                }

                                mSendMessageButton.setEnabled(true);
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                error.printStackTrace();
                                Toast.makeText(ChatActivity.this,
                                        "Não foi possível fazer a requisição, verifique sua conexão com a internet.",
                                        Toast.LENGTH_SHORT)
                                        .show();
                                mSendMessageButton.setEnabled(true);
                            }
                        });
    }

    private void removeChatCommand() {

        mMessage.clearFocus();
        InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mMessage.getWindowToken(), 0);

        mChatContainer.removeAllViews();

        mMessageCounterView.setVisibility(View.INVISIBLE);

        final View chatButtonView = getLayoutInflater().inflate(R.layout.pos_chat_buttons, mChatContainer, false);

        ImageButton acceptButton = (ImageButton) chatButtonView.findViewById(R.id.button_chat_accept);
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                DeviceDelayRequest.init(ChatActivity.this);
//                if(v.isEnabled()) {
//                    DeviceDelayRequest.validate(ChatActivity.this);
//                }
//                v.setEnabled(false);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        v.setEnabled(true);
//                    }
//                }, 4000);
            }
        };

        acceptButton.setOnClickListener(listener);

        chatButtonView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

        mMessagesList.setPadding(
                mMessagesList.getPaddingLeft(),
                mMessagesList.getPaddingTop(),
                mMessagesList.getPaddingRight(),
                mMessagesList.getPaddingBottom()+chatButtonView.getMeasuredHeight());

        RelativeLayout.LayoutParams chatButtonParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        chatButtonParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        chatButtonParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        mRootView.addView(chatButtonView, chatButtonParams);

    }

    private void downloadMessages() {

        BaoClient.getChatService()
                .getMessages("Token " + SessionManager.getActiveSession().getToken(),
                        receiverId,
                        new Callback<List<ChatMessage>>() {
                            @Override
                            public void success(List<ChatMessage> messages, Response response) {
                                mAdapter.add(messages);
                                mAdapter.notifyDataSetChanged();
                                mMessagesList.smoothScrollToPosition(mAdapter.getItemCount());
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                error.printStackTrace();
                                Toast.makeText(ChatActivity.this,
                                        "Não foi possível recuperar as mensagens, verifique sua conexão com a internet.",
                                        Toast.LENGTH_SHORT)
                                        .show();
                            }
                        });
    }


    public void appendMessage(ChatMessage message) {
        mAdapter.add(message);
        if(inBackground)
            NotificationUtils.showChatNotification(this);
        mAdapter.notifyDataSetChanged();
        mMessagesList.smoothScrollToPosition(mAdapter.getItemCount());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == DelayActivity.BACK_TO_BROWSE) {
            startActivity(new Intent(this, GuestsActivity.class));
            finish();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        showExitDialog();
    }

    private void showExitDialog() {
        final LinearLayout layout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.dialog_exit, null);
        ImageView imageView = (ImageView) layout.findViewById(R.id.dialog_image);
        Button yesButton = (Button) layout.findViewById(R.id.yes_button);
        Button cancelButton = (Button) layout.findViewById(R.id.cancel_button);
        Picasso.with(this)
                .load(SessionManager.getActiveSession().getLastChatPicture())
                .transform(new CircleTransform())
                .into(imageView);
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setView(layout);

        final AlertDialog alert = builder.create();
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        alert.show();
    }

    @Override
    public void onDeviceDelayReceived(float delay) {
        finish();
    }

    @Subscribe
    public void onWebsocketMessage(WebsocketManager.WebsocketMessageEvent event) {
        try {
            ChatMessage chatMessage = new ObjectMapper().readValue(event.getMessage(), ChatMessage.class);
            if(chatMessage == null || chatMessage.getMessage() == null)
                return;
            if(chatMessage.getSender().equals(String.valueOf(receiverId)))
                appendMessage(chatMessage);
        } catch (IOException e) {
            // e.printStackTrace();
        }
    }
}

