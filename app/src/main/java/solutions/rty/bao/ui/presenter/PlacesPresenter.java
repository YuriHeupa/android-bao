package solutions.rty.bao.ui.presenter;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;

import com.github.pwittchen.networkevents.library.event.ConnectivityChanged;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import java.util.ArrayList;
import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Style;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import solutions.rty.bao.rest.FoursquareClient;
import solutions.rty.bao.rest.models.foursquare.Venue;

public class PlacesPresenter implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private final Bus bus;
    private final PlacesView view;

    private Location lastLocation;

    // These settings are the same as the settings for the map. They will in fact give you updates
    // at the maximal rates currently possible.
    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(5000)         // 5 seconds
            .setFastestInterval(16)    // 16ms = 60fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    private static final int SPLASH_TIME_OUT = 3000;

    private GoogleApiClient mGoogleApiClient;

    FoursquareClient foursquareClient = new FoursquareClient();


    public interface PlacesView {
        public void setVenues(List<Venue> venues);
        public void setRefreshing(boolean refreshing);
        public void showMessagePanel(String message, Style style);
        public void moveMapCamera(CameraUpdate cameraUpdate);
        public void showMessageToast(String message);
        public Context getContext();
    }

    public PlacesPresenter(PlacesView view, Bus bus) {
        this.view = view;
        this.bus = bus;

        mGoogleApiClient = new GoogleApiClient.Builder(view.getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public void onStart() {
        mGoogleApiClient.connect();
    }

    public void onStop() {
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                        this);
                mGoogleApiClient.disconnect();
            }
        }
    }

    public void onResume() {
        bus.register(this);
    }

    public void onPause() {
        bus.unregister(this);
    }

    public void requestPlaces() {
        if(lastLocation == null) {
            view.showMessageToast("Não foi possível obter sua localização");
            return;
        }
        view.setRefreshing(true);

        foursquareClient.searchForVenues(new Callback<List<Venue>>() {
            @Override
            public void success(List<Venue> venues, Response response) {
                view.setRefreshing(false);
                List<Venue> filteredVenues = new ArrayList<>(CollectionUtils.select(venues, new Predicate<Venue>() {
                    @Override
                    public boolean evaluate(Venue object) {
                        return object.getLocation() != null && !TextUtils.isEmpty(object.getLocation().getAddress());
                    }
                }));
                view.setVenues(filteredVenues);
            }

            @Override
            public void failure(RetrofitError error) {
                view.setRefreshing(false);
                view.showMessagePanel("Não foi possível fazer a requisição, verifique sua conexão com a internet.", Style.ALERT);
            }
        }, lastLocation, 50, 1000, "4d4b7105d754a06376d81259");

    }

    @Override
    public void onConnected(Bundle bundle) {
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if(mLastLocation != null) {
            onLocationChanged(mLastLocation);
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    REQUEST,
                    this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(final Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
        view.moveMapCamera(cameraUpdate);
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        lastLocation = location;
        requestPlaces();
    }


    @Subscribe
    public void onConnectivityChanged(ConnectivityChanged event) {
        switch (event.getConnectivityStatus()) {
            case WIFI_CONNECTED_HAS_INTERNET:
//                view.showMessagePanel("Conectado", Style.CONFIRM);
                if(mGoogleApiClient.isConnected())
                    onConnected(null);
                break;
            case OFFLINE:
//                view.showMessagePanel("Não há conexão com a internet", Style.ALERT);
                break;
        }
    }


}
