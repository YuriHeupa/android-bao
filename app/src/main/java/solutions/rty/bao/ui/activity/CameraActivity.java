package solutions.rty.bao.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import solutions.rty.bao.R;
import solutions.rty.bao.ui.widget.CameraFragment;
import solutions.rty.bao.ui.widget.CameraFragmentListener;

public class CameraActivity extends ActionBarActivity implements CameraFragmentListener {
    /**
     * Tag used on log messages.
     */
    static final String TAG = CameraActivity.class.getSimpleName();

    @Override
    public void onCameraError() {

    }

    Bitmap bitmapTaken;
    CameraFragment cameraFragment;

    @Override
    public void onPictureTaken(Bitmap bitmap) {
        mTakePhotoButton.setImageResource(R.drawable.button_confirm_photo);
        bitmapTaken = bitmap;

        cameraStatus = CAMERA_STATUS.PHOTO_TAKEN;

        FileOutputStream out = null;
        try {
            out = new FileOutputStream(pictureFile);
            bitmapTaken.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private enum CAMERA_STATUS {
        IDLE,
        BUSY,
        PHOTO_TAKEN
    }
    private CAMERA_STATUS cameraStatus = CAMERA_STATUS.IDLE;

    @InjectView(R.id.take_photo_button)
    ImageButton mTakePhotoButton;
    @InjectView(R.id.toolbar)
    Toolbar mToolbar;


    File pictureFile;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        ButterKnife.inject(this);

        this.setSupportActionBar(mToolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cameraFragment = (CameraFragment) getFragmentManager().findFragmentById(R.id.camera_fragment);
        if(!cameraFragment.setCameraFacing(Camera.CameraInfo.CAMERA_FACING_FRONT)) {
            cameraFragment.setCameraFacing(Camera.CameraInfo.CAMERA_FACING_BACK);
        }
        pictureFile = new File(getIntent().getData().getPath());

        mTakePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (cameraStatus) {
                    case IDLE:
                        cameraFragment.takePicture();
                        cameraStatus = CAMERA_STATUS.BUSY;
                        break;
                    case PHOTO_TAKEN:
                        setResult(Activity.RESULT_OK, new Intent().setData(Uri.fromFile(pictureFile)));
                        finish();
                        break;
                }

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.camera, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if(cameraStatus == CAMERA_STATUS.PHOTO_TAKEN) {
            mTakePhotoButton.setImageResource(R.drawable.button_take_photo);
            cameraFragment.startCameraPreview();
            cameraStatus = CAMERA_STATUS.IDLE;
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(Camera.getNumberOfCameras() <= 1)
            menu.removeItem(R.id.menu_camera_switch);
        MenuItem flashItem = menu.findItem(R.id.menu_camera_flash);
        if(!cameraFragment.isFlashModeSupported()) {
            menu.removeItem(flashItem.getItemId());
        } else {
            flashItem.setIcon(cameraFragment.getFlashMode().equals(Camera.Parameters.FLASH_MODE_OFF)
                    ? R.drawable.menu_camera_flash_off : R.drawable.menu_camera_flash_on);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menu_camera_switch:
                if(cameraFragment.getCameraInfo().facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    cameraFragment.setCameraFacing(Camera.CameraInfo.CAMERA_FACING_FRONT);
                } else {
                    cameraFragment.setCameraFacing(Camera.CameraInfo.CAMERA_FACING_BACK);
                }
                invalidateOptionsMenu();
                return true;
            case R.id.menu_camera_flash:
                if(cameraFragment.isFlashModeSupported()) {
                    if (cameraFragment.getFlashMode().equals(Camera.Parameters.FLASH_MODE_TORCH)) {
                        cameraFragment.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    } else {
                        cameraFragment.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    }
                }
                invalidateOptionsMenu();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
