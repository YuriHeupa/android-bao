package solutions.rty.bao.ui.presenter;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Style;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import solutions.rty.bao.data.SessionManager;
import solutions.rty.bao.receiver.WebsocketManager;
import solutions.rty.bao.rest.models.Guest;

import static solutions.rty.bao.data.SessionManager.getActiveSession;
import static solutions.rty.bao.rest.BaoClient.getApiService;

public class GuestsPresenter {

    private static final String TAG = GuestsPresenter.class.getSimpleName();

    private final Bus bus;
    private final GuestsView view;

    private List<Guest> mGuests = new ArrayList<>();

    private boolean activityInBackground;

    public interface GuestsView {
        public void showReadyDialog(final int guestId, final String targetPicture);
        public void setGuests(List<Guest> guests);
        public void setRefreshing(boolean refreshing);
        public void showMessagePanel(String message, Style style);
        public void showInfiniteMessagePanel(String message, Style style);
        public void showMessageToast(String message);
        public boolean isFilteringHighlited();
        public void showMatchNotification();
    }

    public GuestsPresenter(GuestsView view, Bus bus) {
        this.view = view;
        this.bus = bus;
        requestGuests();
        bus.register(this);
    }

    public void onResume() {
        activityInBackground = false;
    }

    public void onPause() {
        activityInBackground = true;
    }

    public void onDestroy() {
        bus.unregister(this);
    }


    public void refreshGuests() {
        if (view.isFilteringHighlited()) {
            view.setGuests(
                    new ArrayList<>(CollectionUtils.select(mGuests, new Predicate<Guest>() {
                        @Override
                        public boolean evaluate(Guest object) {
                            return object.isHighlighted();
                        }
                    }))
            );
        } else {
            view.setGuests(mGuests);
        }
    }

    public void requestGuests() {
        view.setRefreshing(true);

        getApiService().getPlaceGuests("Token " + SessionManager.getActiveSession().getToken(),
                getActiveSession().getPlaceId(),
                new Callback<List<Guest>>() {
                    @Override
                    public void success(List<Guest> guests, Response response) {
                        view.setRefreshing(false);
                        mGuests = guests;
                        refreshGuests();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        view.setRefreshing(false);
                        view.showMessagePanel("Não foi possível fazer a requisição, verifique sua conexão com a internet.", Style.ALERT);
                    }


                });
    }

    @Subscribe
    public void onWebsocketMessage(WebsocketManager.WebsocketMessageEvent event) {
        try {
            JSONObject messageObject = new JSONObject(event.getMessage());
            if(messageObject.has("match")) {
                if(activityInBackground)
                    view.showMatchNotification();
                view.showReadyDialog(messageObject.getInt("match"), messageObject.getString("picture"));
            }
//            if(messageObject.has("ready")) {
//                view.showReadyDialog(messageObject.getInt("ready"), messageObject.getString("picture"));
//            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onWebsocketConnecting(WebsocketManager.WebsocketConnectingEvent event) {
        view.showMessagePanel("Conectando...", Style.INFO);
    }

    @Subscribe
    public void onWebsocketClose(WebsocketManager.WebsocketCloseEvent event) {
        view.showInfiniteMessagePanel("Desconectado", Style.ALERT);
    }

    @Subscribe
    public void onWebsocketOpen(WebsocketManager.WebsocketOpenEvent event) {
        view.showMessagePanel("Conectado", Style.CONFIRM);
    }

}
