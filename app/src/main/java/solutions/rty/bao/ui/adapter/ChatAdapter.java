package solutions.rty.bao.ui.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import solutions.rty.bao.R;
import solutions.rty.bao.data.SessionManager;
import solutions.rty.bao.rest.models.ChatMessage;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 2;
    private static final int TYPE_ITEM = 1;

    private static final String DATE_FORMAT = "hh:mm:ss dd-MM-yyyy";
    private final List<ChatMessage> chatMessages;
    private Activity context;

    public ChatAdapter(Activity context, ArrayList<ChatMessage> chatMessages) {
        this.context = context;
        this.chatMessages = chatMessages;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            final View view = LayoutInflater.from(context).inflate(R.layout.list_item_message, parent, false);
            return new ItemViewHolder(view);
        } else if (viewType == TYPE_HEADER) {
            final View view = LayoutInflater.from(context).inflate(R.layout.chat_header_message, parent, false);
            return new HeaderViewHolder(view);
        }
        throw new RuntimeException("There is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (!isPositionHeader(position)) {
            ItemViewHolder holder = (ItemViewHolder) viewHolder;
            ChatMessage chatMessage = chatMessages.get(position - 1);
            boolean isIncoming = !String.valueOf(SessionManager.getActiveSession().getId()).equals(chatMessage.getSender());
            setAlignment(holder, isIncoming);
            holder.txtMessage.setText(chatMessage.getMessage());
            holder.txtInfo.setText(getTimeText(chatMessage));
            int textColor = isIncoming ? android.R.color.darker_gray : android.R.color.white;
            holder.txtMessage.setTextColor(context.getResources().getColor(textColor));
        }
    }

    @Override
    public int getItemCount() {
        return chatMessages.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    public void add(ChatMessage message) {
        chatMessages.add(message);
        Collections.sort(chatMessages, new Comparator<ChatMessage>() {
            @Override
            public int compare(ChatMessage lhs, ChatMessage rhs) {
                return lhs.getSendAt().compareTo(rhs.getSendAt());
            }
        });
    }

    public void add(List<ChatMessage> messages) {
        chatMessages.addAll(messages);
        Collections.sort(chatMessages, new Comparator<ChatMessage>() {
            @Override
            public int compare(ChatMessage lhs, ChatMessage rhs) {
                return lhs.getSendAt().compareTo(rhs.getSendAt());
            }
        });
    }

    private void setAlignment(ItemViewHolder holder, boolean isIncoming) {
        if (isIncoming) {
            holder.contentWithBG.setBackgroundResource(R.drawable.incoming_message_bg);

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.START;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            holder.content.setLayoutParams(lp);
            layoutParams = (LinearLayout.LayoutParams) holder.txtMessage.getLayoutParams();
            layoutParams.gravity = Gravity.START;
            holder.txtMessage.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtInfo.getLayoutParams();
            layoutParams.gravity = Gravity.START;
            holder.txtInfo.setLayoutParams(layoutParams);
        } else {
            holder.contentWithBG.setBackgroundResource(R.drawable.outgoing_message_bg);

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.END;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            holder.content.setLayoutParams(lp);
            layoutParams = (LinearLayout.LayoutParams) holder.txtMessage.getLayoutParams();
            layoutParams.gravity = Gravity.END;
            holder.txtMessage.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtInfo.getLayoutParams();
            layoutParams.gravity = Gravity.END;
            holder.txtInfo.setLayoutParams(layoutParams);
        }
    }

    private String getTimeText(ChatMessage message) {
        return DateFormat.format(DATE_FORMAT, message.getSendAt().getTime()).toString();
    }


    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @InjectView(R.id.txtMessage)
        public TextView txtMessage;
        @InjectView(R.id.txtInfo)
        public TextView txtInfo;
        @InjectView(R.id.content)
        public LinearLayout content;
        @InjectView(R.id.contentWithBackground)
        public LinearLayout contentWithBG;


        public ItemViewHolder(final View v) {
            super(v);
            ButterKnife.inject(this, v);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
//            final String placeId = mVenues.get(getAdapterPosition()).getId();
//            SessionManager.getActiveSession().setPlaceId(placeId);
//            mContext.startActivity(new Intent(mContext, PlaceRegisterActivity.class));
        }
    }

}