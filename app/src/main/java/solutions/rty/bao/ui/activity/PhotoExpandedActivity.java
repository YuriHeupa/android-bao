package solutions.rty.bao.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import solutions.rty.bao.R;

public class PhotoExpandedActivity extends AppCompatActivity {
    /**
     * Tag used on log messages.
     */
    static final String TAG = PhotoExpandedActivity.class.getSimpleName();

    @InjectView(R.id.picture)
    ImageView mPictureView;
    @InjectView(R.id.toolbar)
    Toolbar mToolbar;

    String picturePath;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_expanded);
        ButterKnife.inject(this);

        final Bundle extras = getIntent().getExtras();
        if(extras != null) {
            picturePath = extras.getString("picture");
            if (picturePath == null)
                finish();
        } else {
            finish();
        }
        Picasso.with(this)
                .load(picturePath)
                .placeholder(android.R.color.darker_gray)
                .into(mPictureView);

        this.setSupportActionBar(mToolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
