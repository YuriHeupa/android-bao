package solutions.rty.bao.receiver;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import solutions.rty.bao.ui.activity.PlacesActivity;
import solutions.rty.bao.R;

/**
 * Created by Yuri Heupa on 12/12/14.
 * Copyright (C) 2013 - All Rights Reserved
 */
public class GcmIntentService extends IntentService {

    private final static String TAG = "GcmIntentService";
    public static final int NOTIFICATION_ID = 1;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        System.out.println(extras.toString());
        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            switch (messageType) {
                case GoogleCloudMessaging.
                        MESSAGE_TYPE_SEND_ERROR:
                    sendNotification("Send error: " + extras.toString());
                    break;
                case GoogleCloudMessaging.
                        MESSAGE_TYPE_DELETED:
                    sendNotification("Deleted messages on server: " +
                            extras.toString());
                    // If it's a regular GCM message, do some work.
                    break;
                case GoogleCloudMessaging.
                        MESSAGE_TYPE_MESSAGE:
                    // Post notification of received message.
                    if (extras.getString("message") != null) {
//                    try {
//                        ChatMessage chatMessage = new ObjectMapper().readValue(extras.getString("message"), ChatMessage.class);
//                        sendChatNotification(chatMessage.chat);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
                        sendNotification(extras.getString("message"));
                    }

                    Log.i(TAG, "Received: " + extras.toString());
                    break;
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }


    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg) {
        NotificationManager mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, PlacesActivity.class), 0);

        Notification.Builder mBuilder =
                new Notification.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setStyle(new Notification.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

//        mBuilder.setVibrate(new long[] { 500, 200, 500, 200, 500, 200, 500, 200});

//        //LED
//        mBuilder.setLights(Color.RED, 3000, 3000);
//
//        String soundUri = "android.resource://"
//                + getPackageName() + "/" + R.raw.funk;
////        Ton
//        mBuilder.setSound(Uri.parse(soundUri));


        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        System.out.println(msg);
    }
}