package solutions.rty.bao.receiver;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import solutions.rty.bao.Config;

/**
 * Created by Yuri Heupa on 15/12/14.
 * Copyright (C) 2013 - All Rights Reserved
 */
public class GCMHandler {

    /**
     * Tag used on log messages.
     */
    static final String TAG = "GCMHandler";

    private static String regid;

    private static GoogleCloudMessaging gcm;

    private static Context context;

    /**
     * Constructor method
     * @param context application's context.
     */
    public static void instantiate(Context context, OnGCMDeviceRegistered callback) {
        GCMHandler.context = context;
        gcm = GoogleCloudMessaging.getInstance(context);
        registerInBackground(callback);
    }

    public interface OnGCMDeviceRegistered {
        public void onGCMDeviceRegistered(String regId);
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private static void registerInBackground(final OnGCMDeviceRegistered callback) {

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg;
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(Config.GCM_SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;
                    callback.onGCMDeviceRegistered(regid);


                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }
        }.execute(null, null, null);
    }


//    /**
//     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
//     * or CCS to send messages to your app. Not needed for this demo since the
//     * device sends upstream messages to a server that echoes back the message
//     * using the 'from' address in the message.
//     */
//    private void sendRegistrationIdToBackend() {
//
//
//        String url = Config.GCM_URL + "register/";
//
//
//        StringEntity entity;
//        try {
//            entity = new StringEntity(getGcmJson(), HTTP.UTF_8);
//            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//            return;
//        }
//
//        client.post(null, url, entity, "application/json", new TextHttpResponseHandler() {
//            @Override
//            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                Log.w("HTTP", String.valueOf(statusCode));
//                if(responseString != null)
//                    System.out.println(responseString);
//                throwable.printStackTrace();
//            }
//
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                Log.w("HTTP", String.valueOf(statusCode));
//                if(responseString != null)
//                    System.out.println(responseString);
//
//                // For this demo: we don't need to send it because the device
//                // will send upstream messages to a server that echo back the
//                // message using the 'from' address in the message.
//
//                // Persist the regID - no need to register again.
//                storeRegistrationId(regid);
//            }
//        });
//
//    }
//
//    public String getGcmJson() {
//        String deviceID = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
//        return JSONUtils.serialize(new GcmDeviceModel(deviceID, regid));
//    }

}
