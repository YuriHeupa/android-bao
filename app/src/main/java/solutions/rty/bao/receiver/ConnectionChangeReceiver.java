// Copyright (c) 2015. RTY Solutions. All rights reserved

package solutions.rty.bao.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.ArrayList;

public class ConnectionChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo != null) {
            for(OnConnectionChange listener : listeners) {
                if(listener != null)
                    listener.onConnectionChange(networkInfo);
            }
//            Toast.makeText(context, "Network is connected: " + (networkInfo.getState() == NetworkInfo.State.CONNECTED ? "Yes" : "No"), Toast.LENGTH_SHORT).show();
//            Toast.makeText(context, "Active Network Type : " + networkInfo.getTypeName(), Toast.LENGTH_SHORT).show();
        }
    }

    private static ArrayList<OnConnectionChange> listeners = new ArrayList<>();

    public static void addOnConnectionChangeListener(OnConnectionChange listener) {
        if(!listeners.contains(listener))
           listeners.add(listener);
    }

    public static void removeOnConnectionChangeListener(OnConnectionChange listener) {
        if(listeners.contains(listener))
            listeners.remove(listener);
    }



    public interface OnConnectionChange {
        public void onConnectionChange(NetworkInfo networkInfo);
    }
}