package solutions.rty.bao.receiver;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.github.pwittchen.networkevents.library.event.ConnectivityChanged;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by Yuri Heupa on 16/04/15.
 * <p/>
 * Copyright (C) 2015 RTY Solutions - All Rights Reserved
 */
public class WebsocketManager {

    private final static String TAG = WebsocketManager.class.getSimpleName();

    private WebSocketClient mWebSocketClient;

    private static WebsocketManager manager;

    private String websocketUrl;

    private Bus bus;

    public WebsocketManager(String websocketUrl, Bus bus) {
        this.websocketUrl = websocketUrl;
        this.bus = bus;
        bus.register(this);
    }

    public static WebsocketManager init(String url, Bus bus) {
        if(manager == null)
            manager = new WebsocketManager(url, bus);
        return manager;
    }

    @Subscribe
    public void onConnectivityChanged(ConnectivityChanged event) {
        switch (event.getConnectivityStatus()) {
            case WIFI_CONNECTED_HAS_INTERNET:
                setupWebsocket();
                break;
        }
    }

    private void setupWebsocket() {
        if(mWebSocketClient != null && mWebSocketClient.getReadyState() == WebSocket.READYSTATE.OPEN)
            return;
        try {
            Log.d(TAG, websocketUrl);
            mWebSocketClient = new WebSocketClient(new URI(websocketUrl)) {

                @Override
                public void onOpen(ServerHandshake serverHandshake) {
                    Log.d(TAG, "OnOpen");
                    post(new WebsocketOpenEvent(serverHandshake));
                }

                @Override
                public void onMessage(String s) {
                    Log.d(TAG, "OnMessage " + s);
                    post(new WebsocketMessageEvent(s));
                }

                @Override
                public void onClose(int i, String s, boolean remote) {
                    Log.d(TAG, "onClose status: " + i + " message: " + s + " remote: " + remote);
                    post(new WebsocketCloseEvent(i, s, remote));
//                    if(remote) {
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                setupWebsocket();
                            }
                        }, 5000);
//                    }
                }

                @Override
                public void onError(Exception e) {
                    Log.e(TAG, "onError", e);
                }
            };
            post(new WebsocketConnectingEvent());

            mWebSocketClient.connect();
        } catch (URISyntaxException e) {
            throw new IllegalStateException("Something wrong with the websocket url", e);
        }
    }

    private final Handler mHandler = new Handler(Looper.getMainLooper());

    public void post(final Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            bus.post(event);
        } else {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    bus.post(event);
                }
            });
        }
    }

//    public synchronized void close() {
//        mWebSocketClient.close();
//        RTYApplication.getBus().register(this);
//    }

    public static class WebsocketCloseEvent {
        int code;
        String reason;
        boolean remote;

        public WebsocketCloseEvent(int code, String reason, boolean remote) {
            this.code = code;
            this.reason = reason;
            this.remote = remote;
        }

        public int getCode() {
            return code;
        }

        public String getReason() {
            return reason;
        }

        public boolean isRemote() {
            return remote;
        }
    }

    public static class WebsocketConnectingEvent {

        public WebsocketConnectingEvent() {

        }
    }

    public static class WebsocketOpenEvent {
        ServerHandshake serverHandshake;

        public WebsocketOpenEvent(ServerHandshake serverHandshake) {
            this.serverHandshake = serverHandshake;
        }

        public ServerHandshake getServerHandshake() {
            return serverHandshake;
        }
    }

    public static class WebsocketMessageEvent {
        String message;

        public WebsocketMessageEvent(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
}
