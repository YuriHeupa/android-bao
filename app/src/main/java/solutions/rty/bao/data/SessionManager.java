package solutions.rty.bao.data;

import android.content.Context;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;

import java.sql.SQLException;
import java.util.List;
import solutions.rty.bao.rest.models.Session;

import static solutions.rty.bao.BaoApplication.getHelper;

/**
 * Created by Yuri Heupa on 20/01/15.
 * Copyright (C) 2013 - All Rights Reserved
 */
public class SessionManager {

    private static Session mSessionInstance;

    public static Session getActiveSession() {
        if (mSessionInstance == null) {
            try {
                Session session = getHelper().getSessionDao().queryBuilder().orderBy("id", false).where().not().eq("id", 0).queryForFirst();
                if(session != null) {
                    mSessionInstance = session;
                } else {
                    List<Session> sessions = getHelper().getSessionDao().queryForAll();
                    mSessionInstance = sessions.isEmpty() ? null : sessions.get(0);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mSessionInstance;
    }

    public static void saveSession() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    getHelper().getSessionDao().deleteById(mSessionInstance.getId());
                    getHelper().getSessionDao().createOrUpdate(mSessionInstance);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    public static void closeActiveSession() {
        if(getActiveSession() == null)
            return;

        try {
            getHelper().getSessionDao().deleteById(mSessionInstance.getId());
            mSessionInstance = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        }

    public static void init(Context context) {
        if(getActiveSession() == null) {
            TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
            Session session = new Session();
            session.setDeviceId(telephonyManager.getDeviceId());
            mSessionInstance = session;
            saveSession();
        }
    }
}
