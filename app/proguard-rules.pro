# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/yuriheupa/Android SDK/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keepnames public class * extends io.realm.RealmObject
-keep class io.realm.** { *; }
-dontwarn javax.**
-dontwarn io.realm.**

#-optimizationpasses 5
#
##When not preverifing in a case-insensitive filing system, such as Windows. Because this tool unpacks your processed jars, you should then use:
#-dontusemixedcaseclassnames
#
##Specifies not to ignore non-public library classes. As of version 4.5, this is the default setting
#-dontskipnonpubliclibraryclasses
#
##Preverification is irrelevant for the dex compiler and the Dalvik VM, so we can switch it off with the -dontpreverify option.
#-dontpreverify
#
##Specifies to write out some more information during processing. If the program terminates with an exception, this option will print out the entire stack trace, instead of just the exception message.
#-verbose

#-dontwarn org.joda.convert.**
#
## Butterknife
#-keep class butterknife.** { *; }
#-dontwarn butterknife.internal.**
#-keep class **$$ViewInjector { *; }
#-keepclasseswithmembernames class * {
#    @butterknife.* <fields>;
#}
#-keepclasseswithmembernames class * {
#    @butterknife.* <methods>;
#}
#
## Jackson
#-dontskipnonpubliclibraryclassmembers
#-keepattributes *Annotation*,EnclosingMethod
#-keepnames class org.codehaus.jackson.** { *; }
#-dontwarn javax.xml.**
#-dontwarn javax.xml.stream.events.**
#-dontwarn com.fasterxml.jackson.databind.**
#
#
## Google Play Services
#-keep class * extends java.util.ListResourceBundle {
#    protected Object[][] getContents();
#}
#
#-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
#    public static final *** NULL;
#}
#
#-keepnames @com.google.android.gms.common.annotation.KeepName class *
#-keepclassmembernames class * {
#    @com.google.android.gms.common.annotation.KeepName *;
#}
#
#-keepnames class * implements android.os.Parcelable {
#    public static final ** CREATOR;
#}
#
## OkHtttp
#-dontwarn rx.**
#
#-dontwarn okio.**
#
#-dontwarn com.squareup.okhttp.**
#-keep class com.squareup.okhttp.** { *; }
#-keep interface com.squareup.okhttp.** { *; }
#
#-keepattributes Signature
#-keepattributes *Annotation*
#
#
#-dontwarn com.caverock.**
#-dontwarn org.joda.time.tz.**
#
##Ormlite
## OrmLite uses reflection
#-keep class com.j256.**
#-keepclassmembers class com.j256.** { *; }
#-keep enum com.j256.**
#-keepclassmembers enum com.j256.** { *; }
#-keep interface com.j256.**
#-keepclassmembers interface com.j256.** { *; }